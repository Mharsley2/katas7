//1

var array1 = ['a', 'b', 'c'];

array1.forEach(function(element) {
  console.log(element);
});


Array.prototype.myForEach =  function(callback) {
    for(var i of this) {
    callback(i);
    }
};

array1.myForEach(function(element) {
    console.log(element);
});

//2


var array2 = [1, 4, 9, 16];


const map1 = array2.map(x => x * 2);

console.log(map1);


Array.prototype.myMap = function(callback) {
    let newArray = []
    for(i of this) {
        newArray.push(callback(i))
    }
    return newArray
}

const map2 = array2.myMap(x => x * 2)



//3

var array = [1, 2, 3, 4, 5];

var isEven = function(element) {

  return element % 2 === 0;
};
var isodd = function(element) {
    
    return element % 2 !== 0;
  };


console.log(array.some(isEven));


var mapArray = [1, 3];

 
Array.prototype.mySome = function(callback) {
    for (let i = 0; i < this.length; i++) {
        if (callback(this[i])) {
            output = true
        }
        return output
    }
}



//4


var array3 = [5, 12, 8, 130, 44]

let found = array3.find(function(element) {
    return element > 10;
})


Array.prototype.myFind = function (callback) {
    for (i of this) {
        if (callback (i)) {
            return i 
        }
    }
    return false
}


//5 

var array1 = [5, 12, 8, 130, 44];

function findFirstLargeNumber(element) {
  return element > 13;
}

console.log(array1.findIndex(findFirstLargeNumber));





Array.prototype.myFindIndex = function (callback) {
    for (i in this) {
        if (callback(this[i])) {
            return i
        }
    }
    return -1
} 


//6 

function isBelowThreshold(currentValue) {
    return currentValue < 40;
  }
  
  var array1 = [1, 30, 39, 29, 10, 13];
  
  console.log(array1.every(isBelowThreshold));
 

  Array.prototype.myEvery = function (callback) {
      for (i of this) {
          if (!callback(value)) {
            return false
          }
      }
      return true
  }

//7 

const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

const result = words.filter(word => word.length > 6);

console.log(result);


Array.prototype.myFilter = function (callback) {
    let newArray = [] 

    for (let value of this) {
        if (callback(value)) {
            newArray.push(value)
        }
    }
return newArray
}


